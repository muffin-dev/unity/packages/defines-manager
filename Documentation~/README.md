# MuffinDev - Defines Manager

Utility for managing define symbols in a Unity project.

## Getting Started

The define symbols are separated in two categories called *scopes*:

- **Project Defines**: Define symbols that are saved in *Player Settings*, and may be platform-specific. These are meant to be shared to other developers.
- **User Defines**: Define symbols that are saved in the `csc.rsp` file, at the root of the `/Assets` directory. It can be used to set parameters to the compiler, including global defines. These are not meant to be shared.

### Ignore `csc.rsp` file

As explained above, **User Defines** are not meant too be shared, and are stored in the `csc.rsp` file of your project. Unless you're using this file for something else than defines, you should add the following lines to your `*.gitignore` file:

```txt
Assets/csc.rsp
Assets/csc.rsp.meta
```

This will ensure the file is not shared with other developers, so you can use **User Defines** as you want without altering the project.

### Manage Defines

Open the *Defines Manager* window from `Tools > Mufifn Dev > Defines Manager`.

The window has two tabs:

- **Project**: This tab allow you to manage **Project Defines** and their target platforms.
- **User**: This tab allow you to manage **User Defines**.

![Project tab view](./Images/project.png)
![User tab view](./Images/user.png)

You can add and remove define symbols by clicking on *+* and *-* buttons at the bottom of the list.

Each element in the list provide these settings:

- A toggle box that allow you to enable/disable define symbols so you don't have to re-add or delete them each time you need it.
- A text field to type the name of the define symbol.
- For **Project Defines**, a dropdown that allow you to select the target platforms of the define symbols.

## API Documentation

- [`Constants`](./constants.md): Miscellaneous constant values used by the Defines Manager.
- [`DefinesManager`](./defines-manager.md): Utility class for managing define symbols for booth project and user scopes.
- [`DefineSymbol`](./define-symbol.md): Represents a define symbol.
- [`DefineSymbolsCollection`](./define-symbols-collection.md): Represents a collection of [`DefineSymbol`](./define-symbol.md).
- [`DefineSymbolsCollectionUtility`](./define-symbols-collection-utility.md): Miscellaneous function for working with [`DefineSymbolsCollection`](./define-symbols-collection.md).
- [`EDefinePlatform`](./define-platform.md): Represents a define symbol platform, based on [`BuildTargetGroup`](https://docs.unity3d.com/ScriptReference/BuildTargetGroup.html), but with obsolete platforms removed and using flags.
- [`EDefinePlatformExtensions`](./define-platform-extensions.md): Extension functions for working with [`EDefinePlatform`](./define-platform.md) values.
- [`EDefineScope`](./define-scope.md): Defines the scope of a define symbol.

---

[<= Back to package's homepage](../README.md)