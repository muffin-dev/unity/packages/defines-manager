# Defines Manager - API Documentation - `Constants`

Miscellaneous constant values used by the Defines Manager.

## Public API

```cs
public static class Constants { }
```

### Fields

#### `RSP_FILE_PATH`

```cs
public const string RSP_FILE_PATH = "Assets/csc.rsp";
```

The base path of the `*.rsp` file used to store global defines, relative to the project absolute path.

#### `LIB_DEFINE_PREFIX`

```cs
public const string LIB_DEFINE_PREFIX = "MUFFINDEV_";
```

The prefix used for the library define symbols.

---

[<= Back to summary](./README.md)