# Defines Manager - API Documentation - `EDefinePlatformExtensions`

Extension functions for working with [`EDefinePlatform`](./define-platform.md) values.

## Public API

```cs
public static class EDefinePlatformExtensions { }
```

### Functions

#### `ToDefinePlatform()`

```cs
public static EDefinePlatform ToDefinePlatform(this BuildTargetGroup buildTargetGroup)
```

Converts the given Unity's [`BuildTargetGroup`](https://docs.unity3d.com/ScriptReference/BuildTargetGroup.html) into a [`EDefinePlatform`](./define-platform.md) value.

- `this BuildTargetGroup buildTargetGroup`: The value to convert.

Returns the converted value.

#### `ToBuildTargetGroup()`

```cs
public static BuildTargetGroup ToBuildTargetGroup(this EDefinePlatform platform)
```

Converts the given Unity's [`BuildTargetGroup`](https://docs.unity3d.com/ScriptReference/BuildTargetGroup.html) into a [`EDefinePlatform`](./define-platform.md) value.

- `this EDefinePlatform platform`: The value to convert.

Returns the converted value.

---

[<= Back to summary](./README.md)