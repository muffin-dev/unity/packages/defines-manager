# Defines Manager - API Documentation - `EDefineScope`

Defines the scope of a define symbol.

## Public API

```cs
public enum EDefineScope
{
  Project,
  User
}
```

### Items

#### `Project`

The define symbol is stored in `PlayerSettings`.

#### `User`

The define symbol is stored in `csc.rsp` file.

---

[<= Back to summary](./README.md)