# Defines Manager - API Documentation - `DefineSymbol`

Represents a define symbol.

## Public API

```cs
[System.Serializable]
public class DefineSymbol { }
```

### Constructors

```cs
public DefineSymbol() { }
```

Defaul constructor.

---

```cs
public DefineSymbol(string name, EDefineScope scope)
```

Creates a new define symbol with given name and scope.

- `string name`: The name of the define symbol, as used in scripts.
- `EDefineScope scope`: The scope of the define symbol.

---

```cs
public DefineSymbol(string name, EDefinePlatform platforms)
```

Creates a new project define symbol with given name and target platforms.

- `string name`: The name of the define symbol, as used in scripts.
- `EDefinePlatform platforms`: The platforms to which this define symbol is applied.

### Functions

#### `AddPlatform()`

```cs
public void AddPlatform(EDefinePlatform platform)
```

Adds a platform to which this define symbol is applied.

- `EDefinePlatform platform`: The target platform you want to add.

#### `NormalizeName()`

```cs
public static string NormalizeName(string define)
```

Normalizes the name of a define symbol.

- `string define`: The define symbol name you want to normalize.

Returns the normalized name of the define symbol.

#### `RemovePlatform()`

```cs
public void RemovePlatform(EDefinePlatform platform)
```

Removes a platform to which this define symbol is applied.

- `EDefinePlatform platform`: The target platform you want to remove.

#### `Validate()`

```cs
public bool Validate()
```

Checks if this define symbol has a valid normalized name and targets at least one platform for project scope.

### Properties

#### `Enabled`

```cs
public bool Enabled { get; set; }
```

If disabled, this define will be saved on disk without being applied to the project.

#### `HasValidName`

```cs
public bool HasValidName { get; set; }
```

Checks if this define symbol has a valid name.

#### `Name`

```cs
public string Name { get; set; }
```

The name of this define symbol, as used in scripts.

#### `Platforms`

```cs
public EDefinePlatform Platforms { get; set; }
```

The platform to which the define symbol is applied. Only used for project define symbols.

#### `Scope`

```cs
public EDefineScope Scope { get; set; }
```

The scope of the define symbol.

---

[<= Back to summary](./README.md)