# Defines Manager - API Documentation - `DefineSymbolsCollectionUtility`

Miscellaneous function for working with [`DefineSymbolsCollection`](./define-symbols-collection.md).

## Public API

```cs
public static class DefineSymbolsCollectionUtility { }
```

### Functions

#### `Save()`

```cs
public static bool Save(DefineSymbolsCollection collection, string path)
```

Saves a define symbols collection to a JSON file.

- `DefineSymbolsCollection collection`: The collection you want to save.
- `string path`: The path to the file in which you want to save the collection. If relative path given, it's resolved from the project absolute path.

Returns `true` if the collection has been saved successfully, otherwise `false`.

#### `Load()`

```cs
public static bool Load(string path, out DefineSymbolsCollection collection)
```

Loads a define symbols collection from a JSON file.

- `string path`: The path to the file that contains the collection you want to load.
- `out DefineSymbolsCollection collection`: Outputs the loaded collection.

Returns `true` if the collection has been loaded successfully, otherwise `false`.

---

[<= Back to summary](./README.md)