# Defines Manager - API Documentation - `DefineSymbolsCollection`

Represents a collection of [`DefineSymbol`](./define-symbol.md).

## Public API

```cs
[System.Serializable]
public class DefineSymbolsCollection : IEnumerable<DefineSymbol> { }
```

### Functions

#### `Add()`

```cs
public bool Add(DefineSymbol defineSymbol)
```

Adds the given define symbol to the list.

- `DefineSymbol defineSymbol`: The define symbol you want to add.

Returns `true` if the define symbol has been added successfully, otherwise `false`.

#### `AddProjectDefine()`

```cs
public bool AddProjectDefine(string defineName, EDefinePlatform platform);
public bool AddProjectDefine(string defineName, EDefinePlatform platforms, out DefineSymbol defineSymbol);
```

Adds the named project define to the list.

- `string defineName`: The define symbol you want to add.
- `EDefinePlatform platforms`: The expected target platform(s) of the define symbol. You must set at least one platform to make the define symbol valid.
- `out DefineSymbol defineSymbol`: Outputs the informations of the added define symbol.

Returns `true` if the define symbol has been added successfully, otherwise `false`.

#### `AddUserDefine()`

```cs
public bool AddUserDefine(string defineName);
public bool AddUserDefine(string defineName, out DefineSymbol defineSymbol);
```

Adds the named user define to the list.

- `string defineName`: The define symbol you want to add.
- `out DefineSymbol defineSymbol`: Outputs the informations of the added define symbol.

Returns `true` if the define symbol has been added successfully, otherwise `false`.

#### `Clean()`

```cs
public void Clean()
```

Remove doubles and invalid entries, and sorts the define symbols list by name.

#### `Find()`

```cs
public DefineSymbol Find(string defineName);
public bool Find(string defineName, out DefineSymbol defineSymbol);
```

Finds the named define symbol informations.

- `string defineName`: The name of the define symbol you want to find.
- `out DefineSymbol defineSymbol`: Outputs the found define symbol informations.

Returns the informations of the found define symbol, or `true` if it has been found, otherwise `false`.

#### `Filter()`

```cs
public List<DefineSymbol> Filter(Predicate<DefineSymbol> filter)
```

Extracts a new list that contains filtered define symbols.

- `Predicate<DefineSymbol> filter`: The function to use to filter items.

Returns the filtered define symbols.

#### `FilterProjectDefines()`

```cs
public List<DefineSymbol> FilterProjectDefines()
```

Extracts the list of all the valid project define symbols.

Returns the filtered list.

#### `FilterUserDefines()`

```cs
public List<DefineSymbol> FilterUserDefines()
```

Extracts the list of all the valid user define symbols.

Returns the filtered list.

#### `Import()`

```cs
public bool Import(DefineSymbolsCollection other)
```

Adds the define symbols of the given collection into this one.

- `DefineSymbolsCollection other`: The other collection to import.

Returns `true` if all the items have been imported successfully, otherwise `false`.

#### `Remove()`

```cs
public bool Remove(string defineName)
```

Removes the named define symbol from the list.

- `string defineName`: The name of the define symbol you want to remove.

Returns `true` if the named define symbol has been removed successfully, otherwise `false`.

#### `RemoveAll()`

```cs
public bool RemoveAll()
```

Removes all the define symbols from this collection.

#### `ToArray()`

```cs
public bool ToArray()
```

Gets the list of define symbols as an array.

---

[<= Back to summary](./README.md)