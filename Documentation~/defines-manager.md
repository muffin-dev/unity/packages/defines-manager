# Defines Manager - API Documentation - `DefinesManager`

Utility class for managing define symbols for booth project and user scopes.

## Public API

```cs
[InitializeOnLoad]
public static class DefinesManager { }
```

### Functions

#### `Add()`

```cs
public static bool Add(DefineSymbol defineSymbol)
```

Adds the given define symbol to the list.

- `DefineSymbol defineSymbol`: The define symbol you want to add.

Returns `true` if the define symbol has been added successfully, otherwise `false`.

#### `AddProjectDefine()`

```cs
public static bool AddProjectDefine(string defineName, EDefinePlatform platform);
public static bool AddProjectDefine(string defineName, EDefinePlatform platforms, out DefineSymbol defineSymbol);
```

Adds the named project define to the list.

- `string defineName`: The define symbol you want to add.
- `EDefinePlatform platforms`: The expected target platform(s) of the define symbol. You must set at least one platform to make the define symbol valid.
- `out DefineSymbol defineSymbol`: Outputs the informations of the added define symbol.

Returns `true` if the define symbol has been added successfully, otherwise `false`.

#### `AddUserDefine()`

```cs
public static bool AddUserDefine(string defineName);
public static bool AddUserDefine(string defineName, out DefineSymbol defineSymbol);
```

Adds the named user define to the list.

- `string defineName`: The define symbol you want to add.
- `out DefineSymbol defineSymbol`: Outputs the informations of the added define symbol.

Returns `true` if the define symbol has been added successfully, otherwise `false`.

#### `Apply()`

```cs
public static void Apply()
```

Apply all changes to define symbols by overriding `PlayerSettings` and `csc.rsp` file.

#### `Find()`

```cs
public static DefineSymbol Find(string defineName);
public static bool Find(string defineName, out DefineSymbol defineSymbol);
```

Finds the named define symbol informations.

- `string defineName`: The name of the define symbol you want to find.
- `out DefineSymbol defineSymbol`: Outputs the found define symbol informations.

Returns the informations of the found define symbol, or `true` if it has been found, otherwise `false`.

#### `Reload()`

```cs
public static void Reload()
```

Loads the define symbols of both project and user scopes. This function should be called only to re-sync the *Defines Manager* when the define symbols have bene modified out of its context.

#### `Remove()`

```cs
public static bool Remove(string defineName)
```

Removes the named define symbol from the list.

- `string defineName`: The name of the define symbol you want to remove.

Returns `true` if the named define symbol has been removed successfully, otherwise `false`.

### Properties

#### `DefineSymbols`

```cs
public static DefineSymbol[] DefineSymbols { get; }
```

The list of loaded define symbols.

---

[<= Back to summary](./README.md)