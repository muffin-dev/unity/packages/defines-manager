/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Miscellaneous constant values used by the Defines Manager.
    /// </summary>
    public static class Constants
    {

        /// <summary>
        /// The base path of the *.rsp file used to store global defines, relative to the project absolute path.
        /// </summary>
        public const string RSP_FILE_PATH = "Assets/csc.rsp";

        /// <summary>
        /// The prefix used for the library define symbols.
        /// </summary>
        public const string LIB_DEFINE_PREFIX = "MUFFINDEV_";

    }

}