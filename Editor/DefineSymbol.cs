/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Represents a define symbol.
    /// </summary>
    [System.Serializable]
    public class DefineSymbol
    {

        #region Fields

        [SerializeField]
        [Tooltip("The name of this define symbol, as used in scripts.")]
        private string _name = string.Empty;

        [SerializeField]
        [Tooltip("The platform to which the define symbol is applied. Only used for project define symbols.")]
        private EDefinePlatform _platforms = EDefinePlatform.Unknown;

        [SerializeField]
        [Tooltip("The scope of the define symbol.")]
        private EDefineScope _scope = EDefineScope.Project;

        [SerializeField]
        [Tooltip("If disabled, this define will be saved on disk without being applied to the project.")]
        private bool _enabled = true;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Defaul constructor.
        /// </summary>
        public DefineSymbol() { }

        /// <summary>
        /// Creates a new define symbol with given name and scope.
        /// </summary>
        /// <param name="name">The name of the define symbol, as used in scripts.</param>
        /// <param name="scope">The scope of the define symbol.</param>
        public DefineSymbol(string name, EDefineScope scope)
        {
            Name = name;
            Scope = scope;
        }

        /// <summary>
        /// Creates a new project define symbol with given name and target platforms.
        /// </summary>
        /// <inheritdoc cref="DefineSymbol(string, EDefineScope)"/>
        /// <param name="platforms">The platforms to which this define symbol is applied.</param>
        public DefineSymbol(string name, EDefinePlatform platforms)
        {
            Name = name;
            Platforms = platforms;
            Scope = EDefineScope.Project;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_name"/>
        public string Name
        {
            get => _name;
            set => _name = NormalizeName(value);
        }

        /// <inheritdoc cref="EDefinePlatform"/>
        public EDefinePlatform Platforms
        {
            get => _platforms;
            set => _platforms = value;
        }

        /// <inheritdoc cref="EDefinePlatform"/>
        public EDefineScope Scope
        {
            get => _scope;
            set => _scope = value;
        }

        /// <inheritdoc cref="_enabled"/>
        public bool Enabled
        {
            get => _enabled;
            set => _enabled = value;
        }

        /// <summary>
        /// Checks if this define symbol has a valid name.
        /// </summary>
        public bool HasValidName
        {
            get
            {
                _name = NormalizeName(_name);
                return !string.IsNullOrEmpty(_name);
            }
        }

        /// <summary>
        /// Normalizes the name of a define symbol.
        /// </summary>
        /// <param name="define">The define symbol name you want to normalize.</param>
        /// <returns>Returns the normalized name of the define symbol.</returns>
        public static string NormalizeName(string define)
        {
            if (define == null)
                define = string.Empty;
            
            return define.Trim();
        }

        /// <summary>
        /// Adds a platform to which this define symbol is applied.
        /// </summary>
        /// <param name="platform">The target platform you want to add.</param>
        public void AddPlatform(EDefinePlatform platform)
        {
            _platforms |= platform;
        }

        /// <summary>
        /// Removes a platform to which this define symbol is applied.
        /// </summary>
        /// <param name="platform">The target platform you want to remove.</param>
        public void RemovePlatform(EDefinePlatform platform)
        {
            if (_platforms.HasFlag(platform))
                _platforms ^= platform;
        }

        /// <summary>
        /// Checks if this define symbol has a valid normalized name and targets at least one platform for project scope.
        /// </summary>
        public bool Validate()
        {
            _name = NormalizeName(_name);
            return !string.IsNullOrEmpty(_name) && (_scope == EDefineScope.User || _platforms != EDefinePlatform.Unknown);
        }

        #endregion

    }

}