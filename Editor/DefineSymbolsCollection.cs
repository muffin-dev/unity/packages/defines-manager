/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using MuffinDev.Core;

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Represents a collection of <see cref="DefineSymbol"/>.
    /// </summary>
    [System.Serializable]
    public class DefineSymbolsCollection : IEnumerable<DefineSymbol>
    {

        #region Fields

        [SerializeField]
        [Tooltip("The list of define symbols in this collection.")]
        private List<DefineSymbol> _defineSymbols = new List<DefineSymbol>();

        #endregion


        #region Public API

        /// <summary>
        /// Adds the given define symbol to the list.
        /// </summary>
        /// <param name="defineSymbol">The define symbol you want to add.</param>
        /// <returns>Returns true if the define symbol has been added successfully, otherwise false.</returns>
        public bool Add(DefineSymbol defineSymbol)
        {
            // Cancel if the given define symbol is not valid
            if (defineSymbol == null || !defineSymbol.HasValidName)
                return false;

            if (!Find(defineSymbol.Name, out _))
            {
                _defineSymbols.Add(defineSymbol);
                return true;
            }

            return false;
        }

        /// <inheritdoc cref="AddProjectDefine(string, EDefinePlatform, out DefineSymbol)"/>
        public bool AddProjectDefine(string defineName, EDefinePlatform platform)
        {
            return AddProjectDefine(defineName, platform, out _);
        }

        /// <summary>
        /// Adds the named project define to the list.
        /// </summary>
        /// <param name="defineName">The define symbol you want to add.</param>
        /// <param name="platforms">The expected target platform(s) of the define symbol. You must set at least one platform to make the
        /// define symbol valid.</param>
        /// <param name="defineSymbol">Outputs the informations of the added define symbol.</param>
        /// <returns>Returns true if the define symbol has been added successfully, otherwise false.</returns>
        public bool AddProjectDefine(string defineName, EDefinePlatform platforms, out DefineSymbol defineSymbol)
        {
            defineSymbol = new DefineSymbol(defineName, platforms);
            if (defineSymbol.HasValidName)
            {
                if (!Find(defineSymbol.Name, out _))
                {
                    _defineSymbols.Add(defineSymbol);
                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc cref="AddUserDefine(string, out DefineSymbol)"/>
        public bool AddUserDefine(string defineName)
        {
            return AddUserDefine(defineName, out _);
        }

        /// <summary>
        /// Adds the named user define to the list.
        /// </summary>
        /// <inheritdoc cref="AddProjectDefine(string, EDefinePlatform, out DefineSymbol)"/>
        public bool AddUserDefine(string defineName, out DefineSymbol defineSymbol)
        {
            defineSymbol = new DefineSymbol(defineName, EDefineScope.User);
            if (defineSymbol.HasValidName)
            {
                if (!Find(defineSymbol.Name, out _))
                {
                    _defineSymbols.Add(defineSymbol);
                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc cref="Find(string, out DefineSymbol)"/>
        /// <returns>Returns the named define symbol informations, or null if it's not in the list.</returns>
        public DefineSymbol Find(string defineName)
        {
            Find(defineName, out DefineSymbol defineSymbol);
            return defineSymbol;
        }

        /// <summary>
        /// Finds the named define symbol informations.
        /// </summary>
        /// <param name="defineName">The name of the define symbol you want to find.</param>
        /// <param name="defineSymbol">Outputs the found define symbol informations.</param>
        /// <returns>Returns true if the named define symbol is in the list, otherwise false.</returns>
        public bool Find(string defineName, out DefineSymbol defineSymbol)
        {
            defineName = DefineSymbol.NormalizeName(defineName);
            defineSymbol = _defineSymbols.Find(ds => ds.Name == defineName);
            return defineSymbol != null;
        }

        /// <summary>
        /// Extracts a new list that contains filtered define symbols.
        /// </summary>
        /// <param name="filter">The function to use to filter items.</param>
        /// <returns>Returns the filtered define symbols.</returns>
        public List<DefineSymbol> Filter(Predicate<DefineSymbol> filter)
        {
            return _defineSymbols.FindAll(filter);
        }

        /// <summary>
        /// Extracts the list of all the valid project define symbols.
        /// </summary>
        /// <returns>Returns the filtered list.</returns>
        public List<DefineSymbol> FilterProjectDefines()
        {
            return _defineSymbols.FindAll(ds => ds.Scope == EDefineScope.Project && ds.HasValidName);
        }

        /// <summary>
        /// Extracts the list of all the valid user define symbols.
        /// </summary>
        /// <returns>Returns the filtered list.</returns>
        public List<DefineSymbol> FilterUserDefines()
        {
            return _defineSymbols.FindAll(ds => ds.Scope == EDefineScope.User && ds.HasValidName);
        }

        /// <summary>
        /// Adds the define symbols of the given collection into this one.
        /// </summary>
        /// <param name="other">The other collection to import.</param>
        /// <returns>Returns true if all the items have been imported successfully, otherwise false.</returns>
        public bool Import(DefineSymbolsCollection other)
        {
            bool success = true;
            foreach (DefineSymbol ds in other)
            {
                if (!Add(ds))
                    success = false;
            }
            return success;
        }

        /// <summary>
        /// Removes the named define symbol from the list.
        /// </summary>
        /// <param name="defineName">The name of the define symbol you want to remove.</param>
        /// <returns>Returns true if the named define symbol has been removed successfully, otherwise false.</returns>
        public bool Remove(string defineName)
        {
            defineName = DefineSymbol.NormalizeName(defineName);
            return _defineSymbols.RemoveAll(ds => ds.Name == defineName) > 0;
        }

        /// <summary>
        /// Removes all the define symbols from this collection.
        /// </summary>
        public void RemoveAll()
        {
            _defineSymbols.Clear();
        }

        /// <summary>
        /// Remove doubles and invalid entries, and sorts the define symbols list by name.
        /// </summary>
        public void Clean()
        {
            _defineSymbols.RemoveAll(ds => !ds.Validate());
            _defineSymbols.RemoveDoubles((a, b) => a.Name == b.Name);
            _defineSymbols.Sort((a, b) => a.Name.CompareTo(b.Name));
        }

        /// <summary>
        /// Iterates through the define symbols in this collection.
        /// </summary>
        public IEnumerator<DefineSymbol> GetEnumerator()
        {
            return ((IEnumerable<DefineSymbol>)_defineSymbols).GetEnumerator();
        }

        /// <inheritdoc cref="GetEnumerator"/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_defineSymbols).GetEnumerator();
        }

        /// <summary>
        /// Gets the list of define symbols as an array.
        /// </summary>
        public DefineSymbol[] ToArray()
        {
            return _defineSymbols.ToArray();
        }

        #endregion

    }

}