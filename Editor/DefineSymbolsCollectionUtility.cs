/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.IO;

using UnityEditor;

using MuffinDev.Core;

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Miscellaneous function for working with <see cref="DefineSymbolsCollection"/>.
    /// </summary>
    public static class DefineSymbolsCollectionUtility
    {

        /// <summary>
        /// Saves a define symbols collection to a JSON file.
        /// </summary>
        /// <param name="collection">The collection you want to save.</param>
        /// <param name="path">The path to the file in which you want to save the collection. If relative path given, it's resolved from
        /// the project absolute path.</param>
        /// <returns>Returns true if the collection has been saved successfully, otherwise false.</returns>
        public static bool Save(DefineSymbolsCollection collection, string path)
        {
            if (string.IsNullOrEmpty(path) || collection == null)
                return false;

            string json = EditorJsonUtility.ToJson(collection, true);
            File.WriteAllText(path.AbsolutePath(), json);
            return true;
        }

        /// <summary>
        /// Loads a define symbols collection from a JSON file.
        /// </summary>
        /// <param name="path">The path to the file that contains the collection you want to load.</param>
        /// <param name="collection">Outputs the loaded collection.</param>
        /// <returns>Returns true if the collection has been loaded successfully, otherwise false.</returns>
        public static bool Load(string path, out DefineSymbolsCollection collection)
        {
            collection = null;
            if (string.IsNullOrEmpty(path))
                return false;

            path = path.AbsolutePath();
            if (File.Exists(path))
            {
                collection = new DefineSymbolsCollection();
                EditorJsonUtility.FromJsonOverwrite(File.ReadAllText(path), collection);
                return true;
            }

            return false;
        }

    }

}