/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using UnityEditor;

using MuffinDev.Core;

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Utility class for managing define symbols for booth project and user scopes.
    /// </summary>
    [InitializeOnLoad]
    public static class DefinesManager
    {

        #region Fields

        private const string RSP_DEFINE_PREFIX = "-define";
        private const string RSP_DEFINE_SEPARATOR = ":";
        private const string DISABLED_PROJECT_DEFINES_PATH = "ProjectSettings/Defines.json";
        private const string DISABLED_USER_DEFINES_PATH = "UserSettings/Defines.json";

        private static string RSPFilePath => Constants.RSP_FILE_PATH.AbsolutePath();

        /// <summary>
        /// The define symbols to setup when Unity starts.
        /// </summary>
        private static readonly string[] StartupUserDefines = new string[]
        {
            $"{Constants.LIB_DEFINE_PREFIX}DEFINES_MANAGER"
        };

        /// <summary>
        /// The list of all build target groups available.
        /// </summary>
        private static readonly BuildTargetGroup[] AvailableBuildTargetGroups = new BuildTargetGroup[]
        {
            BuildTargetGroup.Standalone,
            BuildTargetGroup.iOS,
            BuildTargetGroup.Android,
            BuildTargetGroup.WebGL,
            BuildTargetGroup.WSA,
            BuildTargetGroup.PS4,
            BuildTargetGroup.XboxOne,
            BuildTargetGroup.tvOS,
            BuildTargetGroup.Switch,
            BuildTargetGroup.Stadia,
            BuildTargetGroup.CloudRendering,
            BuildTargetGroup.PS5,
        };

        /// <summary>
        /// Matches a define symbol declaration in an *.rsp file.
        /// </summary>
        private static readonly Regex RSPREgex = new Regex(@"-define\s*:\s*(?<define>[A-Za-z0-9_]+)");

        /// <summary>
        /// The list of loaded define symbols.
        /// </summary>
        private static DefineSymbolsCollection s_defineSymbols = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Static constructor.
        /// </summary>
        static DefinesManager()
        {
            foreach (string d in StartupUserDefines)
                AddUserDefine(d);
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="s_defineSymbols"/>
        public static DefineSymbol[] DefineSymbols => Collection.ToArray();

        /// <summary>
        /// Loads the define symbols of both project and user scopes. This function should be called only to re-sync the Defines Manager
        /// when the define symbols have bene modified out of its context.
        /// </summary>
        public static void Reload()
        {
            Collection.RemoveAll();

            LoadProjectDefines();
            LoadUserDefines();

            Collection.Clean();
        }

        /// <inheritdoc cref="DefineSymbolsCollection.Add(DefineSymbol)"/>
        public static bool Add(DefineSymbol defineSymbol)
        {
            return Collection.Add(defineSymbol);
        }

        /// <inheritdoc cref="DefineSymbolsCollection.Add(string, EDefinePlatform)"/>
        public static bool AddProjectDefine(string defineName, EDefinePlatform platform)
        {
            return Collection.AddProjectDefine(defineName, platform);
        }

        /// <inheritdoc cref="DefineSymbolsCollection.Add(string, EDefinePlatform, out DefineSymbol)"/>
        public static bool AddProjectDefine(string defineName, EDefinePlatform platform, out DefineSymbol defineSymbol)
        {
            return Collection.AddProjectDefine(defineName, platform, out defineSymbol);
        }

        /// <inheritdoc cref="DefineSymbolsCollection.AddUserDefine(string)"/>
        public static bool AddUserDefine(string defineName)
        {
            return Collection.AddUserDefine(defineName);
        }

        /// <inheritdoc cref="DefineSymbolsCollection.AddUserDefine(string, out DefineSymbol)"/>
        public static bool AddUserDefine(string defineName, out DefineSymbol defineSymbol)
        {
            return Collection.AddUserDefine(defineName, out defineSymbol);
        }

        /// <inheritdoc cref="DefineSymbolsCollection.Find(string)"/>
        public static DefineSymbol Find(string defineName)
        {
            return Collection.Find(defineName);
        }

        /// <inheritdoc cref="DefineSymbolsCollection.Find(string, out DefineSymbol)"/>
        public static bool Find(string defineName, out DefineSymbol defineSymbol)
        {
            return Collection.Find(defineName, out defineSymbol);
        }

        /// <inheritdoc cref="DefineSymbolsCollection.Remove(string)"/>
        public static bool Remove(string defineName)
        {
            return Collection.Remove(defineName);
        }

        /// <summary>
        /// Apply all changes to define symbols by overriding <see cref="PlayerSettings"/> and csc.rsp file.
        /// </summary>
        public static void Apply()
        {
            Collection.Clean();

            ApplyProjectDefines();
            ApplyUserDefines();
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="s_defineSymbols"/>
        private static DefineSymbolsCollection Collection
        {
            get
            {
                if (s_defineSymbols == null)
                {
                    s_defineSymbols = new DefineSymbolsCollection();
                    Reload();
                }
                return s_defineSymbols;
            }
        }

        /// <summary>
        /// Tries to get the named define symbol with the expected scope, and create it if it doesn't exist. This function will change the
        /// scope of an existing define symbol only if the expected scope is <see cref="EDefineScope.User"/> (making it global).
        /// </summary>
        /// <param name="defineName">The name of the define symbol you want to find.</param>
        /// <param name="defineSymbol">Outputs the found define symbol informations.</param>
        /// <param name="scope">The expected scope of the define symbol. If the expected scope is <see cref="EDefineScope.User"/>, this
        /// function will ensure the found define symbol has that scope, even if it's already using <see cref="EDefineScope.Project"/>
        /// scope.</param>
        /// <returns>Returns true if the informations have been found or created successfully, otherwise false..</returns>
        private static bool GetOrCreate(string defineName, out DefineSymbol defineSymbol, EDefineScope scope)
        {
            // If the named define exists in the list
            if (Find(defineName, out defineSymbol))
            {
                // Ensure its scope if needed
                if (scope == EDefineScope.User)
                    defineSymbol.Scope = EDefineScope.User;
            }
            // Else, create the define symbol informations, and register it in the list.
            else
            {
                defineSymbol = new DefineSymbol(defineName, scope);
                return Collection.Add(defineSymbol);
            }

            return true;
        }

        /// <summary>
        /// Loads the project define symbols from <see cref="PlayerSettings"/>, and the diabled ones from ProjectSettings directory.
        /// </summary>
        private static void LoadProjectDefines()
        {
            // Load project defines from PlayerSettings
            foreach (BuildTargetGroup group in AvailableBuildTargetGroups)
            {
                EDefinePlatform platform = group.ToDefinePlatform();
                PlayerSettings.GetScriptingDefineSymbolsForGroup(group, out string[] defines);
                foreach (string d in defines)
                {
                    if (GetOrCreate(d, out DefineSymbol info, EDefineScope.Project))
                        info.AddPlatform(platform);
                }
            }

            if (DefineSymbolsCollectionUtility.Load(DISABLED_PROJECT_DEFINES_PATH, out DefineSymbolsCollection disabledDefinesCollection))
                Collection.Import(disabledDefinesCollection);
        }

        /// <summary>
        /// Loads the user define symbols from csc.rsp file, and the disabled ones from UserSettings directory.
        /// </summary>
        private static void LoadUserDefines()
        {
            // Load user defines from csc.rsp file
            if (File.Exists(RSPFilePath))
            {
                // Read csc.rsp lines
                string[] lines = File.ReadAllText(RSPFilePath).SplitLines();
                // For each line
                foreach (string line in lines)
                {
                    Match match = RSPREgex.Match(line);
                    // If the current line contain a define
                    if (match.Success)
                    {
                        // Register the user define if it's valid
                        if (match.Groups["define"].Success && !string.IsNullOrEmpty(match.Groups["define"].Value))
                            GetOrCreate(match.Groups["define"].Value, out _, EDefineScope.User);
                    }
                }
            }

            if (DefineSymbolsCollectionUtility.Load(DISABLED_USER_DEFINES_PATH, out DefineSymbolsCollection disabledDefinesCollection))
                Collection.Import(disabledDefinesCollection);
        }

        /// <summary>
        /// Applies the project defines by overwriting <see cref="PlayerSettings"/>.
        /// </summary>
        private static void ApplyProjectDefines()
        {
            List<DefineSymbol> defineSymbols = Collection.FilterProjectDefines();
            DefineSymbolsCollection disabledDefinesCollection = new DefineSymbolsCollection();

            // Overwrite PlayerSettings with project define symbols
            // For each available build target
            foreach (BuildTargetGroup group in AvailableBuildTargetGroups)
            {
                EDefinePlatform platform = group.ToDefinePlatform();
                List<string> defines = new List<string>();

                // For each project define symbol
                foreach (DefineSymbol ds in defineSymbols)
                {
                    if (ds.Enabled)
                    {
                        if (ds.Platforms.HasFlag(platform))
                            defines.Add(ds.Name);
                    }
                    else
                    {
                        disabledDefinesCollection.Add(ds);
                    }
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(group, defines.ToArray());
            }

            DefineSymbolsCollectionUtility.Save(disabledDefinesCollection, DISABLED_PROJECT_DEFINES_PATH);
        }

        /// <summary>
        /// Applies the user defines by overwriting the csc.rsp file.
        /// </summary>
        private static void ApplyUserDefines()
        {
            List<DefineSymbol> defineSymbols = Collection.FilterUserDefines();
            DefineSymbolsCollection disabledDefinesCollection = new DefineSymbolsCollection();

            // For each user define symbols
            List<string> lines = new List<string>();
            foreach (DefineSymbol ds in defineSymbols)
            {
                if (ds.Enabled)
                    lines.Add($"{RSP_DEFINE_PREFIX}{RSP_DEFINE_SEPARATOR}{ds.Name}");
                else
                    disabledDefinesCollection.Add(ds);
            }
        
            // If there's no user defines to set up, delete the file
            if (lines.Count <= 0)
            {
                AssetDatabase.DeleteAsset(Constants.RSP_FILE_PATH);
            }
            // Else, if there's at least one define line to write
            else
            {
                File.WriteAllLines(RSPFilePath, lines);
                AssetDatabase.ImportAsset(Constants.RSP_FILE_PATH);
            }

            DefineSymbolsCollectionUtility.Save(disabledDefinesCollection, DISABLED_USER_DEFINES_PATH);
        }

        #endregion

    }

}