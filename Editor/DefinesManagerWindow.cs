/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using MuffinDev.Core;
using MuffinDev.Core.EditorOnly;

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Utility window for managing the define symbols of both project and user scopes.
    /// </summary>
    public class DefinesManagerWindow : EditorWindow
    {

        #region Enums

        /// <summary>
        /// Defines a tab of this window.
        /// </summary>
        public enum ETab
        {
            Project,
            User
        }

        #endregion


        #region Fields

        private const string WINDOW_TITLE = "Defines Manager";
        private const string MENU_ITEM = EditorConstants.EDITOR_WINDOW_MENU + "/" + WINDOW_TITLE;

        private const string DEFAULT_NEW_DEFINE_NAME = "NEW_DEFINE";
        private const float PLATFORM_DROPDOWN_WIDTH = MoreGUI.WIDTH_L;

        private static readonly GUIContent[] TargetPlatformLabels = null;

        [SerializeField]
        [Tooltip("The active tab of this window.")]
        private ETab _activeTab = ETab.Project;

        [SerializeField]
        private Vector2 _scrollPosition = Vector2.zero;

        [SerializeField]
        private EDefinePlatform _filterPlatform = EDefinePlatform.All;

        [SerializeField]
        [Tooltip("The current research string used to filter defines.")]
        private string _searchString = string.Empty;

        /// <summary>
        /// The define symbols list used for this editor.
        /// </summary>
        private List<DefineSymbol> _tmpDefineSymbolsList = null;

        /// <summary>
        /// The reorderable list GUI to display the define symbols list of this editor.
        /// </summary>
        private ReorderableList _tmpDefinesReorderableList = null;

        /// <summary>
        /// Flag enabled if a change is waiting to be applied in Defines Manager.
        /// </summary>
        private bool _pendingChanges = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Static constructor.
        /// </summary>
        static DefinesManagerWindow()
        {
            string[] platformNames = Enum.GetNames(typeof(EDefinePlatform));
            List<GUIContent> labels = new List<GUIContent>();

            for (int i = 0; i < platformNames.Length; i++)
            {
                if (i == 0)
                {
                    labels.Add(new GUIContent("All"));
                    continue;
                }

                labels.Add(new GUIContent(platformNames[i]));
            }

            TargetPlatformLabels = labels.ToArray();
        }

        /// <summary>
        /// Called when this window is open.
        /// </summary>
        private void OnEnable()
        {
            SetActiveTab(_activeTab, true);
        }

        #endregion


        #region Public API

        /// <summary>
        /// Opens this editor window.
        /// </summary>
        [MenuItem(MENU_ITEM)]
        public static DefinesManagerWindow Open()
        {
            DefinesManagerWindow window = GetWindow<DefinesManagerWindow>(false, WINDOW_TITLE, true);
            window.Show();
            return window;
        }

        /// <inheritdoc cref="_activeTab"/>
        public ETab ActiveTab
        {
            get => _activeTab;
            set => SetActiveTab(value);
        }

        /// <summary>
        /// Sets the active tab of this window.
        /// </summary>
        /// <param name="tab">The new active tab of this window.</param>
        /// <param name="force">If enabled, applies the tab change even if the given tab is already the active one.</param>
        public void SetActiveTab(ETab tab, bool force = false)
        {
            // Cancel if the given tab is already the active one
            if (tab == _activeTab && !force)
                return;

            ETab previousTab = _activeTab;
            _activeTab = tab;
            OnActiveTabChange(previousTab, _activeTab);
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this window GUI on screen.
        /// </summary>
        private void OnGUI()
        {
            // Draw tabs toolbar
            ActiveTab = (ETab)GUILayout.Toolbar((int)ActiveTab, Enum.GetNames(typeof(ETab)));

            EditorGUILayout.Space();
            EditorGUI.BeginChangeCheck();
            // Draw filter toolbar
            using (new EditorGUILayout.HorizontalScope())
            {
                _searchString = EditorGUILayout.TextField("Filter", _searchString);
                using (new EnabledScope(ActiveTab == ETab.Project))
                {
                    _filterPlatform = (EDefinePlatform)EditorGUILayout.EnumFlagsField(_filterPlatform, GUILayout.Width(PLATFORM_DROPDOWN_WIDTH));
                }
            }
            if (EditorGUI.EndChangeCheck())
                ReloadDefinesList();

            // Draw defines list
            EditorGUILayout.Space();
            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);
            {
                TmpDefinesReorderableList.DoLayoutList();
            }
            EditorGUILayout.EndScrollView();

            // Draw "apply" button
            using (new EnabledScope(!EditorApplication.isCompiling && _pendingChanges))
            {
                if (GUILayout.Button("Apply Changes", MoreGUI.HeightL))
                {
                    DefinesManager.Apply();
                    ReloadDefinesList();
                    _pendingChanges = false;
                }
            }
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="_tmpDefineSymbolsList"/>
        private List<DefineSymbol> TmpDefineSymbolsList
        {
            get
            {
                // Initialize the list if it's null
                if (_tmpDefineSymbolsList == null)
                    _tmpDefineSymbolsList = new List<DefineSymbol>();

                // Reload the list if it's empty
                if (_tmpDefineSymbolsList.Count == 0)
                {
                    _tmpDefineSymbolsList.AddRange(DefinesManager.DefineSymbols);

                    // Remove all defines that are not used in the selected scope
                    _tmpDefineSymbolsList.RemoveAll(ds => ds.Scope != SelectedScope);

                    // Remove all defines that are not applied to the filtered platforms
                    if (SelectedScope == EDefineScope.Project && (int)_filterPlatform > 0 && _filterPlatform != EDefinePlatform.All)
                        _tmpDefineSymbolsList.RemoveAll(ds => !ds.Platforms.HasFlag(_filterPlatform));

                    // Remove all defines in the list that don't match the search string
                    string search = (_searchString != null ? _searchString.Trim() : string.Empty).ToLower();
                    if (!string.IsNullOrEmpty(search))
                        _tmpDefineSymbolsList.RemoveAll(ds => !ds.Name.ToLower().Contains(search));
                }
                return _tmpDefineSymbolsList;
            }
        }

        /// <inheritdoc cref="_tmpDefinesReorderableList"/>
        private ReorderableList TmpDefinesReorderableList
        {
            get
            {
                if (_tmpDefinesReorderableList == null)
                {
                    _tmpDefinesReorderableList = new ReorderableList(TmpDefineSymbolsList, typeof(DefineSymbol));

                    _tmpDefinesReorderableList.drawHeaderCallback = rect => EditorGUI.LabelField(rect, $"{ActiveTab} Define Symbols");

                    _tmpDefinesReorderableList.drawElementCallback = (rect, index, isActive, isFocused) =>
                    {
                        EditorGUI.BeginChangeCheck();

                        rect.y += (rect.height - EditorGUIUtility.singleLineHeight) / 2;
                        rect.height = EditorGUIUtility.singleLineHeight;

                        Rect tmpRect = new Rect(rect);
                        tmpRect.width = EditorGUIUtility.singleLineHeight;

                        // Draw toggle field
                        TmpDefineSymbolsList[index].Enabled = EditorGUI.Toggle(tmpRect, TmpDefineSymbolsList[index].Enabled);

                        tmpRect.x += tmpRect.width + MoreGUI.H_MARGIN;
                        tmpRect.width = rect.width - EditorGUIUtility.singleLineHeight - MoreGUI.H_MARGIN;
                        // Allow space to display platform dropdown if the item is a project define symbol
                        if (TmpDefineSymbolsList[index].Scope == EDefineScope.Project)
                            tmpRect.width -= PLATFORM_DROPDOWN_WIDTH + MoreGUI.H_MARGIN;

                        // Draw name field
                        TmpDefineSymbolsList[index].Name = EditorGUI.TextField(tmpRect, TmpDefineSymbolsList[index].Name);

                        // Draw platform dropdown if the item is a project define symbol
                        if (TmpDefineSymbolsList[index].Scope == EDefineScope.Project)
                        {
                            tmpRect.x += tmpRect.width + MoreGUI.H_MARGIN;
                            tmpRect.width = PLATFORM_DROPDOWN_WIDTH;
                            TmpDefineSymbolsList[index].Platforms = (EDefinePlatform)EditorGUI.EnumFlagsField(tmpRect, TmpDefineSymbolsList[index].Platforms);
                        }

                        if (EditorGUI.EndChangeCheck())
                            _pendingChanges = true;
                    };

                    _tmpDefinesReorderableList.onAddCallback = list =>
                    {
                        DefineSymbol newDefine = new DefineSymbol
                        {
                            Name = GetUniqueDefineName(),
                            Scope = SelectedScope,
                            Platforms = _filterPlatform != EDefinePlatform.Unknown ? _filterPlatform : EDefinePlatform.All
                        };

                        if (DefinesManager.Add(newDefine))
                        {
                            _searchString = string.Empty;
                            ReloadDefinesList();
                            _pendingChanges = true;
                        }
                    };

                    _tmpDefinesReorderableList.onRemoveCallback = list =>
                    {
                        if (DefinesManager.Remove(TmpDefineSymbolsList[list.index].Name))
                        {
                            ReloadDefinesList();
                            _pendingChanges = true;
                        }
                    };
                }
                return _tmpDefinesReorderableList;
            }
        }

        /// <summary>
        /// Reloads the temporary defines list, and the associated GUI.
        /// </summary>
        private void ReloadDefinesList()
        {
            _tmpDefineSymbolsList = null;
            _tmpDefinesReorderableList = null;
        }

        /// <summary>
        /// Gets the selected scope to display the appropriate define symbols.
        /// </summary>
        private EDefineScope SelectedScope
        {
            get
            {
                switch (ActiveTab)
                {
                    case ETab.User:
                        return EDefineScope.User;

                    default:
                        return EDefineScope.Project;
                }
            }
        }

        /// <summary>
        /// Gets a unique define name, using the default name as a base.
        /// </summary>
        /// <returns>Returns the unique name.</returns>
        private string GetUniqueDefineName()
        {
            int index = 0;
            string uniqueName = DEFAULT_NEW_DEFINE_NAME;
            DefineSymbol[] defines = DefinesManager.DefineSymbols;

            // For each define
            for (int i = 0; i < defines.Length; i++)
            {
                // If the name already exists, make a new unique name
                if (defines[i].Name == uniqueName)
                {
                    index++;
                    uniqueName = $"{DEFAULT_NEW_DEFINE_NAME}_{index}";
                    i = -1;
                }
            }

            return uniqueName;
        }

        /// <summary>
        /// Called when the active tab of this window changes.
        /// </summary>
        private void OnActiveTabChange(ETab previousTab, ETab activeTab)
        {
            _scrollPosition = Vector2.zero;
            ReloadDefinesList();
        }

        #endregion

    }

}