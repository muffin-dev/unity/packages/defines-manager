/**
 * MuffinDev � 2022
 * Author: Hubert GROSSIN - dev@hubertg.fr
 */

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Represents a define symbol platform, based on <see cref="UnityEditor.BuildTargetGroup"/>, but with obsolete platforms removed and
    /// using flags.
    /// </summary>
    [System.Flags]
    public enum EDefinePlatform
    {
        Unknown = 0,

        Standalone = 1 << 0,
        iPhone = 1 << 1, // @warn This may change for "iOS" in the future
        Android = 1 << 2,
        WebGL = 1 << 3,
        WSA = 1 << 4,
        PS4 = 1 << 5,
        XboxOne = 1 << 6,
        tvOS = 1 << 7,
        Switch = 1 << 8,
        Stadia = 1 << 9,
        CloudRendering = 1 << 10,
        PS5 = 1 << 11,

        All = Standalone | iPhone | Android | WebGL | WSA | PS4 | XboxOne | tvOS | Switch | Stadia | CloudRendering | PS5
    }

}