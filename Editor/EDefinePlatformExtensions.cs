/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

using UnityEditor;

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Extension functions for working with <see cref="EDefinePlatform"/> values.
    /// </summary>
    public static class EDefinePlatformExtensions
    {

        /// <summary>
        /// Converts the given Unity's <see cref="BuildTargetGroup"/> into a <see cref="EDefinePlatform"/> value.
        /// </summary>
        /// <param name="buildTargetGroup">The value to convert.</param>
        /// <returns>Returns the converted value.</returns>
        public static EDefinePlatform ToDefinePlatform(this BuildTargetGroup buildTargetGroup)
        {
            // Get available define platform names
            string[] definePlatformNames = Enum.GetNames(typeof(EDefinePlatform));
            // Get the name of the BuildTargetGroup value
            string targetGroupName = Enum.GetName(typeof(BuildTargetGroup), buildTargetGroup);

            // For each define platform constant
            for (int i = 0; i < definePlatformNames.Length; i++)
            {
                // If the define platform name matches with the given target group name
                if (definePlatformNames[i] == targetGroupName)
                {
                    // Get the associated value to the current define platform and return it as EDefinePlatform value
                    Array value = Enum.GetValues(typeof(EDefinePlatform));
                    int j = 0;
                    foreach (int v in value)
                    {
                        if (j == i)
                            return (EDefinePlatform)v;

                        j++;
                    }
                }
            }
            return EDefinePlatform.Unknown;
        }

        /// <summary>
        /// Converts a define platform into a <see cref="BuildTargetGroup"/> value.
        /// </summary>
        /// <param name="platform">The value to convert.</param>
        /// <returns>Returns the converted value.</returns>
        public static BuildTargetGroup ToBuildTargetGroup(this EDefinePlatform platform)
        {
            // Get available build target groups
            string[] buildTargetGroupNames = Enum.GetNames(typeof(BuildTargetGroup));
            // Get the name of the EDefinePlatform value
            string targetPlatformName = Enum.GetName(typeof(EDefinePlatform), platform);

            // For each build target group constant
            for (int i = 0; i < buildTargetGroupNames.Length; i++)
            {
                // If the build target group name matches with the given platform
                if (buildTargetGroupNames[i] == targetPlatformName)
                {
                    // Get the associated value to the current group and return it as BuildTargetGroup value
                    Array value = Enum.GetValues(typeof(BuildTargetGroup));
                    int j = 0;
                    foreach (int v in value)
                    {
                        if (j == i)
                            return (BuildTargetGroup)v;

                        j++;
                    }
                }
            }
            return BuildTargetGroup.Unknown;
        }

    }

}