/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.DefinesManager.EditorOnly
{

    /// <summary>
    /// Defines the scope of a define symbol.
    /// </summary>
    public enum EDefineScope
    {

        /// <summary>
        /// The define symbol is stored in <see cref="UnityEditor.PlayerSettings"/>.
        /// </summary>
        Project,

        /// <summary>
        /// The define symbol is stored in csc.rsp file.
        /// </summary>
        User

    }

}